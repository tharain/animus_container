FROM node:latest

MAINTAINER Daphne Won <won.jr@hotmail.com>

# Add a normal user
RUN useradd --user-group --create-home --shell /bin/bash animus

# Configure environment
ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    HOME=/home/animus

RUN apt-get update -y && apt-get install -y --no-install-recommends apt-utils

# Install some dependencies
RUN apt-get install -y vim less postgresql-client

# Install nodejs for custom build
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -;
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list;
RUN apt-get update && apt-get install -y yarn;
RUN yarn install;

RUN mkdir $HOME/animus-server
RUN mkdir $HOME/animus-mobile-android
RUN mkdir $HOME/animus-web-ui
RUN mkdir $HOME/animus-mobile-ios

WORKDIR $HOME/

copy ./ ./

RUN chown -R animus:animus $HOME

USER animus

HEALTHCHECK CMD ["curl", "-f", "http://localhost:8088/health"]

EXPOSE 8088
