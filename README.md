# animus_container

Type the following commands to get your docker container set-up:

```
git clone https://github.com/tharain/animus_container
cd animus_container
git clone https://github.com/bobby-lin/animus-server
git clone https://github.com/tharain/animus-web-ui
bash docker-build.sh
cd nginx
docker-compose up -d
cd ..
docker-compose up -d
docker network connect animuscontainer_animus nginx-proxy
docker-compose down
docker-compose up -d
docker exec -it animuscontainer_animus_1 bash
```

You will now be inside the container.
To exit, just type `exit`
To shut down the container. Type
```
docker-compose down
```

Fin. for now.
